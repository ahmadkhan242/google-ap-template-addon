function onOpen() {
}
function onInstall() {
}
function showSidebar() {
}
function insertTemp() {
}
function customTemp() {
}
function openDialog() {
}!function(e, a) {
    for (var i in a) e[i] = a[i];
}(this, function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    return __webpack_require__.m = modules, __webpack_require__.c = installedModules, 
    __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value: value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function() {
            return module["default"];
        } : function() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 1);
}([ function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function onInstall() {
        onOpen();
    }
    function onOpen() {
        DocumentApp.getUi().createMenu("Custom scripts").addItem("Templates Dialog", "openDialog").addItem("Templates Sidebar", "showSidebar").addToUi();
    }
    function showSidebar() {
        var html = HtmlService.createTemplateFromFile("main").evaluate().setTitle("Accord Project Template");
        DocumentApp.getUi().showSidebar(html);
    }
    __webpack_require__.d(__webpack_exports__, "d", (function() {
        return onOpen;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return onInstall;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return showSidebar;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return insertTemp;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return customTemp;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return openDialog;
    }));
    var openDialog = function() {
        var html = HtmlService.createHtmlOutputFromFile("mainDialog").setWidth(1e3).setHeight(500);
        DocumentApp.getUi().showModalDialog(html, "Accord Project Template");
    }, insertTemp = function(data) {
        var body = DocumentApp.getActiveDocument().getBody();
        body.appendParagraph(data.title).setHeading(DocumentApp.ParagraphHeading.HEADING1), 
        body.appendParagraph(data.sample), body.appendPageBreak();
    }, customTemp = function(data) {
        var body = DocumentApp.getActiveDocument().getBody();
        body.clear(), body.appendParagraph(data.title).setHeading(DocumentApp.ParagraphHeading.HEADING1), 
        body.appendParagraph(data.sample), body.appendPageBreak();
    };
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), function(global) {
        var _doc_utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
        global.onOpen = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["d"], global.onInstall = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["c"], 
        global.showSidebar = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["f"], global.insertTemp = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["b"], 
        global.customTemp = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["a"], global.openDialog = _doc_utilities__WEBPACK_IMPORTED_MODULE_0__["e"];
    }.call(this, __webpack_require__(2));
}, function(module, exports) {
    var g;
    g = function() {
        return this;
    }();
    try {
        g = g || new Function("return this")();
    } catch (e) {
        "object" == typeof window && (g = window);
    }
    module.exports = g;
} ]));