import React, { Component} from "react";
import {
    Navbar,
    Container,
    Nav,
    Dropdown,
    DropdownButton,
    Button,
    Form,
    Image,
    Modal    
  } from "react-bootstrap";
import server from '../server';
import "../style/main.scss"
import data from "../template.json"


import {
  TITLE,
  SideBar_dropdown,
  SideBar_Form,
  SideBar_Template_Title,
  SideBar_Template_P,
  SideBar_Template_Sample
} from "../style/styled"

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
          template: null,
          employee: null,
          company: null,
          tax: null
        };
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
      }


      onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
      }
      

    sampleDisplay(e){
      console.log(e, "AJAJA");
      this.setState({template: e})
      var Sample = 'Eating healthy clause between "Dan" (the Employee) and "ACME" (the Company). The canteen only sells apple products. Apples, apple juice, apple flapjacks, toffee apples. Employee gets fired if caught eating anything without apples in it. THE EMPLOYEE, IF ALLERGIC TO APPLES, SHALL ALWAYS BE HUNGRY. Apple products at the canteen are subject to a 4.5% tax.'
      server.insertTemp({title:e.name, sample:Sample})
      .then(()=>{
        console.log("Sample is Added");
      })
      .catch((e)=>{
        console.log(e);
      })
    }

    onSubmit(e){
      e.preventDefault()
      var Sample = `Eating healthy clause between ${this.state.employee} (the Employee) and ${this.state.company} (the Company). The canteen only sells apple products. Apples, apple juice, apple flapjacks, toffee apples. Employee gets fired if caught eating anything without apples in it. THE EMPLOYEE, IF ALLERGIC TO APPLES, SHALL ALWAYS BE HUNGRY. Apple products at the canteen are subject to a ${this.state.tax}% tax.`
      server.customTemp({title:this.state.template.name, sample:Sample})
      .then(()=>{
        console.log("Sample is Added");
      })
      .catch((e)=>{
        console.log(e);
      })
    }
    
  render() {
   
    return (
      <div >
        <TITLE>
        AP Templates
        </TITLE>
            <SideBar_dropdown>
              <DropdownButton
                      key="primary"
                      variant="primary"
                      title={this.state.template==null?"Choose Template Here": this.state.template.name}
                    >
                        {data.map((dt)=>{
                    return <Dropdown.Item onSelect={()=>{this.sampleDisplay(dt)}}>{dt.name}@{dt.version}</Dropdown.Item>;
                  })}
              </DropdownButton>
            </SideBar_dropdown>
            <div>
          {this.state.template==null?<p></p>: 
            <div className="template">
                <SideBar_Template_Title>
                  <SideBar_Template_P>Name - {this.state.template.name}</SideBar_Template_P>
                  <SideBar_Template_P>Version - {this.state.template.version}</SideBar_Template_P>
                  <SideBar_Template_Sample> See Sample on The Document page</SideBar_Template_Sample>
                </SideBar_Template_Title>
                <SideBar_Form>
                  <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="formGroupEmail">
                      <Form.Label>Employee</Form.Label>
                      <Form.Control 
                      name="employee"
                      required
                      value={this.state.employee}
                      onChange={this.onChange}
                      type="text"
                      placeholder="Employee name" />
                    </Form.Group>
                    <Form.Group controlId="formGroupEmail">
                      <Form.Label>Company</Form.Label>
                      <Form.Control 
                      name="company"
                      required
                      value={this.state.company}
                      onChange={this.onChange}
                      type="text"
                      placeholder="Company Name" />
                    </Form.Group>
                    <Form.Group controlId="formGroupEmail">
                      <Form.Label>Tax</Form.Label>
                      <Form.Control 
                      name="tax"
                      required
                      value={this.state.tax}
                      onChange={this.onChange}
                      type="number"
                      placeholder="Tax rate" />
                    </Form.Group>
                    <Button variant="primary" type="submit" style={{marginTop:"5px", backgroundColor:"#3AAFA9", border:"0", color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>
                        Submit
                    </Button>
                  </Form>
                </SideBar_Form>
             
             
            </div>
          }
            </div>
           </div>
    );
  }
}

export default Sidebar;
