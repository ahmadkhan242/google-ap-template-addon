import React, { Component} from "react";
import {
    Row,
    Col,
    Nav,
    Dropdown,
    DropdownButton,
    Button,
    Form,
    Image,
    Modal    
  } from "react-bootstrap";
import styled from "styled-components"
import server from '../server';
import "../style/main.scss"
// import data from "../template.json"
import {TemplateLibrary, Template, Clause} from '@accordproject/cicero-core';
import * as ciceroPackageJson from '@accordproject/cicero-core/package.json';

class TempDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
          template: null,
          employee: null,
          company: null,
          tax: null,
          sample: null,
          templatedata:null
        };
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
      }

      onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
      }


    sampleDisplay(e){
      var Sample = 'Eating healthy clause between "Dan" (the Employee) and "ACME" (the Company). The canteen only sells apple products. Apples, apple juice, apple flapjacks, toffee apples. Employee gets fired if caught eating anything without apples in it. THE EMPLOYEE, IF ALLERGIC TO APPLES, SHALL ALWAYS BE HUNGRY. Apple products at the canteen are subject to a 4.5% tax.'
      
      this.setState({template: e,sample:Sample})
      
    }

    showSample(){
      var Sample = `Eating healthy clause between ${this.state.employee} (the Employee) and ${this.state.company} (the Company). The canteen only sells apple products. Apples, apple juice, apple flapjacks, toffee apples. Employee gets fired if caught eating anything without apples in it. THE EMPLOYEE, IF ALLERGIC TO APPLES, SHALL ALWAYS BE HUNGRY. Apple products at the canteen are subject to a ${this.state.tax}% tax.`
      server.customTemp({title:this.state.template.name, sample:Sample})
      .then(()=>{
        console.log("Sample is Added");
      })
      .catch((e)=>{
        console.log(e);
      })
      
    }
    onSubmit(e){
      e.preventDefault()
      var Sample = `Eating healthy clause between ${this.state.employee} (the Employee) and ${this.state.company} (the Company). The canteen only sells apple products. Apples, apple juice, apple flapjacks, toffee apples. Employee gets fired if caught eating anything without apples in it. THE EMPLOYEE, IF ALLERGIC TO APPLES, SHALL ALWAYS BE HUNGRY. Apple products at the canteen are subject to a ${this.state.tax}% tax.`
      this.setState({sample:Sample})
    }

    componentDidMount() {
      const ciceroVersion = ciceroPackageJson.version;
      const templateLibrary = new TemplateLibrary();
      const tempData = []
      templateLibrary.getTemplateIndex({ latestVersion: true, ciceroVersion })
      .then((res)=>{
          
          for (const t in res) {
              if (Object.prototype.hasOwnProperty.call(res, t)) {
                tempData.push({name: res[t].displayName, url: res[t].url, version: res[t].version});
              }
          }
      })
      .then(()=>{
        this.setState({templatedata: tempData})
      })
    }

  render() {    
    return (
      <div >
            <p className="title">AP Templates</p>
            <div className="dialogropdown">
            <DropdownButton
                key="primary"
                variant="primary"
                title={this.state.template==null?"Choose Template Here": this.state.template.name}
              >
                {this.state.templatedata==null? <Dropdown.Item></Dropdown.Item> : this.state.templatedata.map((dt)=>{
            return <Dropdown.Item onSelect={()=>{this.sampleDisplay(dt)}}>{dt.name}@{dt.version}</Dropdown.Item>;
           })}
        </DropdownButton>
            </div>
            <Row>
              <Col>
              <div>
                {this.state.template==null?<p></p>: 
                  <div className="dialogTemplate">
                    <div className="dialogTemplate__title">
                      <p className="dialogTemplate__title__name">Name - {this.state.template.name}</p>
                      <p className="dialogTemplate__title__version">Version - {this.state.template.version}</p>
                      <p className="dialogTemplate__title__sample"> See Sample on Your Right Side</p>
                    </div>
                    <div className="template__form">
                    <Form onSubmit={this.onSubmit}>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label>Employee</Form.Label>
                        <Form.Control 
                        name="employee"
                        required
                        value={this.state.employee}
                        onChange={this.onChange}
                        type="text"
                        placeholder="Employee name" />
                      </Form.Group>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label>Company</Form.Label>
                        <Form.Control 
                        name="company"
                        required
                        value={this.state.company}
                        onChange={this.onChange}
                        type="text"
                        placeholder="Company Name" />
                      </Form.Group>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label>Tax</Form.Label>
                        <Form.Control 
                        name="tax"
                        required
                        value={this.state.tax}
                        onChange={this.onChange}
                        type="number"
                        placeholder="Tax rate" />
                      </Form.Group>
                      <Button variant="primary" type="submit" style={{marginTop:"5px", border:"0", color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>
                          Submit
                      </Button>
                    </Form>
                    </div>
                  </div>
                }
                  </div>
              </Col>
              <Col>
              {this.state.sample==null? <p></p>:
              <div>
              <div className="dialogSample">
                <h3 className="dialogSample__title">{this.state.template.name}</h3>
                  {this.state.sample}
                </div>
                <Button onClick={this.showSample()}>ADD</Button></div>
                }
              </Col>
            </Row>
           </div>
    );
  }
}

export default TempDialog;
