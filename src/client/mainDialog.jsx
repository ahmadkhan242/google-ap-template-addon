import React from 'react';
import ReactDOM from 'react-dom';
import TempDialog from "./components/TempDialog"


ReactDOM.render(
  <TempDialog />,
  document.getElementById('index')
);
