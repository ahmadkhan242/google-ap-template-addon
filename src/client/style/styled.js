import styled from "styled-components";
const TITLE = styled.p`
text-align: center;
font-weight: bold;
`;
const SideBar_dropdown = styled.div`
    width: 150px;
    margin-left: 60px;
`
const SideBar_Form = styled.div`
    margin: 10px;
`
const SideBar_Template_Title = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 10px;
`
const SideBar_Template_P = styled.p`
    margin-left: 10px;
    font-size: 15px;
    font-weight: bold;
`
const SideBar_Template_Sample = styled.p`
    margin-left: 10px;
    font-size: 15px;
    font-weight: bold;
`

export {
    TITLE,
    SideBar_dropdown,
    SideBar_Form,
    SideBar_Template_Title,
    SideBar_Template_P,
    SideBar_Template_Sample
}