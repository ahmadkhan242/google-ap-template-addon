function onInstall() {
  onOpen();
}

function onOpen() {
  DocumentApp.getUi()
  .createMenu('Custom scripts')
  .addItem('Templates Dialog', 'openDialog')
  .addItem("Templates Sidebar", "showSidebar")
  .addToUi();  // Run the showSidebar function when someone clicks the menu
}

function showSidebar() {
  var html = HtmlService.createTemplateFromFile("main")
    .evaluate()
    .setTitle("Accord Project Template"); // The title shows in the sidebar
  DocumentApp.getUi().showSidebar(html);
}

const openDialog = () => {
  const html = HtmlService.createHtmlOutputFromFile('mainDialog')
    .setWidth(1000)
    .setHeight(500);
    DocumentApp.getUi() // Or DocumentApp or FormApp.
    .showModalDialog(html, 'Accord Project Template');
};

const insertTemp = data =>  {
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  var par1 = body.appendParagraph(data.title);
  par1.setHeading(DocumentApp.ParagraphHeading.HEADING1);
  body.appendParagraph(data.sample);
  body.appendPageBreak();
}

const customTemp = data =>{
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();
  body.clear();
  var par1 = body.appendParagraph(data.title);
  par1.setHeading(DocumentApp.ParagraphHeading.HEADING1);
  body.appendParagraph(data.sample);
  body.appendPageBreak();
}


export {
  onOpen,
  onInstall,
  showSidebar,
  insertTemp,
  customTemp,
  openDialog
};
