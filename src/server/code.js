import * as publicFunctions from './doc-utilities';

// Expose public functions
global.onOpen = publicFunctions.onOpen;
global.onInstall = publicFunctions.onInstall;
global.showSidebar = publicFunctions.showSidebar;
global.insertTemp = publicFunctions.insertTemp;
global.customTemp = publicFunctions.customTemp;
global.openDialog = publicFunctions.openDialog;
