# Google AP Template Addon
>This Project is Implementation of React framework on Google Docs Add-on to diaplay AP Templates. 

# Installation
- Clone This project and Install dependencies
    
    ```
    > git clone https://gitlab.com/ahmadkhan242/google-ap-template-addon.git
    > cd google-ap-template-addon
    > npm install
    ```
- Enable the Google Apps Script API ([script.google.com/home/usersettings](script.google.com/home/usersettings))

- Log in to `clasp` to manage your Google Apps Scripts from the command line

    ```
    > npm run login 
    ```
    
- Create a new Google docs file and find the script's scriptId by opening your docs, `selecting Tools > Script Editor, then File > Project properties`.

- Create `.clasp.json` file and add following code
    ```
    // .clasp.json
    {"rootDir": "dist", "scriptId":"...paste scriptId here..."}
    
    ```
- Run this code to deploy it on current docs file
    ```
    npm run deploy
    ```
- You will see a custom scripts button on main menu bar.